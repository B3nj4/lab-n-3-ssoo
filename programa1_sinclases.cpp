
/*
 * g++ -o programa1 programa1_sinclases.cpp -lpthread
 * uso: ./programa1 archivo.txt archivo2.txt
 */

#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <chrono>

//#include "procesos.h"
using namespace std;

// Calculos en funciones
void *cuenta_linea(void *param); 
void *cuenta_caracter(void *param);
void *cuenta_palabra(void *param);

void *procesa_archivos(void *param) {

    // Variables para abrir el archivo
    char *archivo;
    archivo = (char *) param;

    // Apertura del archivo
    ifstream file(archivo);
    
    cuenta_caracter(archivo);
    cuenta_linea(archivo); 
    cuenta_palabra(archivo);
    return 0;
    
}

int main(int argc, char *argv[]) {
    
    //Procesos procesos;
    pthread_t threads[argc - 1];
    int i = 0;

    /* Crea todos los hilos */
    auto start = chrono::system_clock::now();

    for (i=0; i < argc - 1; i++) {
        cout << "Nombre del archivo: " << argv[i+1] << endl;
        
        pthread_create(&threads[i], NULL, procesa_archivos, argv[i+1]);
        pthread_join(threads[i], NULL);
        
        cout << endl;
    }

    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << "Time in seconds: " << elapsed_seconds.count() << endl;

    return 0;
}


// FUncion para contar lineas
void *cuenta_linea(void *param) {
    
    // Variables para abrir el archivo
    char *archivo;
    archivo = (char *) param;

    // Apertura del archivo
    ifstream file(archivo);
    
    // Variables
    string str;
    int cnt = 0;

    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

    }

    cout << cnt << " lineas " << endl;
    return 0;

}

// FUncion para contar caracter
void *cuenta_caracter(void *param) {
    
     // Variables para abrir el archivo
    char *archivo;
    archivo = (char *) param;

    // Apertura del archivo
    ifstream file(archivo);
    
    // Variables
    string str;
    int cnt = 0;
    int cnt_car = 0;
    
    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

        for(int i = 0; i < str.length(); i++){
            cout << str[i];
            // Condiciones para contar caracteres sin considerar espacios
            if(str[i] != ' '){
                cnt_car++;
            }
        }

    }

    cout << "\n" << cnt_car << " caracteres " << endl;

    return 0;
}

// Funcion para contar palabras
void *cuenta_palabra(void *param) {
    
     // Variables para abrir el archivo
    char *archivo;
    archivo = (char *) param;

    // Apertura del archivo
    ifstream file(archivo);
    
    // Variables
    int cnt = 0;
    string str;
    int cnt_pal = 0;

    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

        for(int i = 0; i < str.length(); i++){
            // Condiciones para contar palabras
            if(str[i+1] == ' ' && str[i-1] != ' '){
                cnt_pal++;
            }
        }

    }

    cout << cnt_pal << " palabras " << endl;
    return 0;
}