#ifndef PROCESOS_H
#define PROCESOS_H

//Librerias
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
using namespace std;

// Clase procesos
class Procesos {
    private:
        
    public:
        // Constructor
        Procesos();

        // Metodos
        
        /* Gestiona el archivo junto con las demas funciones de conteo lineas,
        caracteres y palabras*/
        void *procesa_archivos(void *param);
    
        /* Conteo */
        void cuenta_linea(string param); 
        void cuenta_caracter(string param);
        void cuenta_palabra(string param);

        //int programa1(int argc, char *argv[]);
        //int programa2(int argc, char *argv[]);
        
};
#endif
