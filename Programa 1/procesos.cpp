#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>

#include "procesos.h"
using namespace std;


void *Procesos::procesa_archivos(void *param) {

    // Variables para abrir el archivo
    string archivo;
    archivo = (char *) param;

    // Apertura del archivo
    ifstream file(archivo);
    
    // FUnciones de conteo
    cuenta_caracter(archivo);
    cuenta_linea(archivo); 
    cuenta_palabra(archivo);
    
} 

// FUncion para contar lineas
void Procesos::cuenta_linea(string param) {
    
    // Variables para abrir el archivo
    //char *archivo;
    //archivo = (char *) param;

    // Apertura del archivo
    ifstream file(param);
    
    // Variables y contadores (cnt)
    string str;
    int cnt = 0;

    // Recorre lineas para contarlas
    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

    }

    cout << cnt << " lineas " << endl;

}

// FUncion para contar caracter
void Procesos::cuenta_caracter(string param) {
    
    // Variables para abrir el archivo
    //char *archivo;
    //archivo = (char *) param;

    // Apertura del archivo
    ifstream file(param);
    
    // Variables y contadores (cnt)
    string str;
    int cnt = 0;
    int cnt_car = 0;
    
    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

        for(int i = 0; i < str.length(); i++){
            cout << str[i];
            // Condiciones para contar caracteres sin considerar espacios
            if(str[i] != ' '){
                cnt_car++;
            }
        }

    }

    cout << "\n" << cnt_car << " caracteres " << endl;
    
}

// Funcion para contar palabras
void Procesos::cuenta_palabra(string param) {
    
    // Variables para abrir el archivo
    //char *archivo;
    //archivo = (char *) param;

    // Apertura del archivo
    ifstream file(param);
    
    // Variables y contadores (cnt)
    int cnt = 0;
    string str;
    int cnt_pal = 0;

    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

        // Analiza por cada linea las palabras mediante algoritmo
        for(int i = 0; i < str.length(); i++){
            // Condiciones para contar palabras
            if(str[i+1] == ' ' && str[i-1] != ' '){
                cnt_pal++;
            }
        }

    }

    cout << cnt_pal << " palabras " << endl;
}
