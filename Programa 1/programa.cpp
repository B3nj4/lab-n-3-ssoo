#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <chrono>

#include "procesos.h"
using namespace std;

// Calculos en funciones
//void *cuenta_linea(void *param); 
//void *cuenta_caracter(void *param);
//void *cuenta_palabra(void *param);

int main(int argc, char *argv[]) {    
    
    cout << "\nPrograma 1: ";
    
    // Variables
    Procesos procesos;
    pthread_t threads[argc - 1];
    int i = 0;

    // Temporizador del inicio del proceso
    auto start = chrono::system_clock::now();

    // Crea las hebras necesarias condensadas en una funcion "procesa_archivos"
    for (i=0; i < argc - 1; i++) {
        cout << "Nombre del archivo: " << argv[i+1] << endl;
        
        pthread_create(&threads[i], NULL, procesos.procesa_archivos, argv[i+1]);
        pthread_join(threads[i], NULL);
        
        cout << endl;
    }

    return 0;
    
    // Temporiza el fin del proceso
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    
    // Impresion del tiempo
    cout << "time: second: " << elapsed_seconds.count() << endl; 


    return 0;
}