# Lab Nº3 - SSOO 

Dicho laboratorio pretende ejecutar los conocimientos adquiridos en torno a hebras, escencialmente, el uso de los comandos pthread_Creates y pthread_join -y sus variaciones- 
para confrontar diferentes retos. En este caso, se requiere comparar dos programas que al ingresarseles archivos de textos (.txt) lograran contar las
letras, caracteres y saltos de linea de dichos archivos. Ademas, compararemos el tiempo de funcionamiento de estos programas, ya que, el primero (programa2) trata de ejecutar los metodos de contabilidad con atributos y hebras, mientras que el segundo programa (programa3) hace dichos metodos con
una funcion, hebras y sin atributos, lo anterior ocn la finalidad de encontrar el proceso mas eficiente.  

## Requisitos de instalacion

- [ ] Este proyecto requiere la instalacion de las dependencias mediante el comando git clone [url repositorio], luego para utilizarlo debemos cerciorarnos que 
estemos dentro del proyecto y bastaria solo con realizar la linea de comando "make" dentro del
programa a utilizar siendo el Programa 1 el que necesitara la combinacion "cd 'Programa 1' " y luego "./programa [archivos]" para su ejecucion. Siempre para cumplir esta dependencia "/lab-n-3-ssoo/Programa 1$" , por ejemplo:

```
git remote clone https://gitlab.com/B3nj4/lab-n-3-ssoo

cd 'Programa 1'

make

./programa [nombre del primer archivo en .txt] [nombre del segundo archivo en .txt] ... [nombre del n archivo .txt]

```

En caso de querer entrar al segundo programa deberemos salir del programa 1 (siempre y cuando estemos en él) con la combinacion "cd .." y luego entrar con "cd Programa 2" y agregar el make correspondiente. EStas combinaciones pueden usarse para ingresar tanto al "Programa 1" como el "Programa 2". (/lab-n-3-ssoo/Programa 2$)

```

cd ..

make

./programa [nombre del primer archivo en .txt] [nombre del segundo archivo en .txt] ... [nombre del n archivo .txt]

```



## Herramientas

- [ ] Realizado en la IDE Visual Studio Code
- [ ] Uso de paquetes <pthread.h>
- [ ] Uso de paquetes <chronos.h>
- [ ] Para realizar el proyecto, se utilizo el lenguaje de programacion C++


## Autor
Benjamin Vera Garrido - Ingenieria Civil en Bioinformatica
