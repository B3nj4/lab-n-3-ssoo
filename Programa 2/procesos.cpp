#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>

#include "procesos.h"
using namespace std;


void Procesos::procesa_archivos(string param) {

    // Variables para abrir el archivo
    //string archivo;
    //archivo = (char *) param;

    // Apertura del archivo
    ifstream file(param);
    
    cuenta_caracter(param);
    cuenta_linea(param); 
    cuenta_palabra(param);
    
} 

// FUncion para contar lineas
void Procesos::cuenta_linea(string param) {
    
    // Variables para abrir el archivo
    //char *archivo;
    //archivo = (char *) param;

    // Apertura del archivo
    ifstream file(param);
    
    // Variables
    string str;
    int cnt = 0;

    // REcorre cada linea y la contabiliza
    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

    }

    cout << cnt << " lineas " << endl;

}

// FUncion para contar caracter
void Procesos::cuenta_caracter(string param) {
    
    // Variables para abrir el archivo
    //char *archivo;
    //archivo = (char *) param;

    // Apertura del archivo
    ifstream file(param);
    
    // Variables
    string str;
    int cnt = 0;
    int cnt_car = 0;
    int i = 0;

    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

        // Analiza por cada caracter mediante algoritmo y lo contabiliza
        for(i = 0; i < str.length(); i++){
            cout << str[i];
            // Condiciones para contar caracteres sin considerar espacios
            if(str[i] != ' '){
                cnt_car++;
            }
        }

    }

    cout << "\n" << cnt_car << " caracteres " << endl;
    
}

// Funcion para contar palabras
void Procesos::cuenta_palabra(string param) {
    
    // Variables para abrir el archivo
    //char *archivo;
    //archivo = (char *) param;

    // Apertura del archivo
    ifstream file(param);
    
    // Variables
    int cnt = 0;
    string str;
    int cnt_pal = 0;

    while (getline(file, str)) {
        
        // Contador de lineas
        cnt++;

        // Analiza por cada linea las palabras mediante algoritmo
        for(int i = 0; i < str.length(); i++){
            // Condiciones para contar palabras
            if(str[i+1] == ' ' && str[i-1] != ' '){
                cnt_pal++;
            }
        }

    }

    cout << cnt_pal << " palabras " << endl;
}




/* 
// PROGRAMA 2
int *Procesos::programa2(int argc, char *argv[]){
    
    int i = 0;
    
    for (i=0; i < argc - 1; i++) {
        cout << "Nombre del archivo: " << argv[i+1] << endl;
        procesa_archivos(argv[i+1]);
        
        cout << endl;
    }
    
    return 0;
} */
