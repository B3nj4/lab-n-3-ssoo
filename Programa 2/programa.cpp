#include <pthread.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <chrono>

#include "procesos.h"
using namespace std;

int main(int argc, char *argv[]) {    
    
    cout << "\nPrograma 2: ";
    Procesos proceso;
    string archivo;
    
    auto start = chrono::system_clock::now(); 

    for(int i=0; i < argc - 1; i++) {
        archivo = argc -1;

        cout << "Nombre del archivo: " << archivo << endl;
        proceso.procesa_archivos(archivo);
        
        cout << endl;
    }
    
    auto end = chrono::system_clock::now();
    chrono::duration<double> elapsed_seconds = end-start;
    cout << "time: second: " << elapsed_seconds.count() << endl; 
    
    
    return 0;
}